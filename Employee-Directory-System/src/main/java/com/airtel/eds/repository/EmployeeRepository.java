package com.airtel.eds.repository;

import com.airtel.eds.model.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee,Long>{
   public List<Employee> findByFirstName(String name);
}
